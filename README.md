# Ghost 0.11.x Cloudron App

This repository contains the Cloudron app package source for [Ghost](https://ghost.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.ghost.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.ghost.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd ghost-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the posts are still ok.

```
cd ghost-app/test

npm install
USERNAME=<username> PASSWORD=<password> mocha --bail test.js
```

Note that Ghost is not integrated with Cloudron user management. As a result, the above credentials are _not_ the cloudron credentials.

