// # Ghost Configuration
// Setup your Ghost install for various environments
// Documentation can be found at http://docs.ghost.org/usage/configuration/

var path = require('path'),
    os = require('os'),
    config;

var pgUrl = require('url').parse(process.env.POSTGRESQL_URL);

config = {
    // ### Production
    // When running Ghost in the wild, use the production environment
    // Configure your URL and mail settings here
    production: {
        url: process.env.APP_ORIGIN,
        mail: {
            transport: 'SMTP',
            from: process.env.MAIL_FROM,
            options: {
                host: process.env.MAIL_SMTP_SERVER,
                port: parseInt(process.env.MAIL_SMTP_PORT, 10),
                auth: {
                    user: process.env.MAIL_SMTP_USERNAME,
                    pass: process.env.MAIL_SMTP_PASSWORD
                }
            }
        },
        database: {
            client: 'pg',
            connection: {
                host     : pgUrl.hostname,
                user     : pgUrl.auth.split(':')[0],
                password : pgUrl.auth.split(':')[1],
                database : pgUrl.path.substring(1), // remove the '/'
                charset  : 'utf8'
            }
        },
        server: {
            // Host to be passed to node's `net.Server#listen()`
            host: '0.0.0.0',
            // Port to be passed to node's `net.Server#listen()`, for iisnode set this to `process.env.PORT`
            port: '2368'
        },
        privacy: {
            useUpdateCheck: false,
            useGoogleFonts: true,
            useGravatar: true,
            useRpcPing: false,
            useStructuredData: true
        },
        paths: {
            contentPath: '/app/data/content/'
        }
    }
};

// Export config
module.exports = config;
