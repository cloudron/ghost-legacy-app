#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Key = require('selenium-webdriver').Key;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var firefox = require('selenium-webdriver/chrome');
    var server, browser = new firefox.Driver();
    var LOCATION = 'test';
    var app, email;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    function setup(done) {
        browser.manage().deleteAllCookies();
        browser.get('https://' + app.fqdn).then(function () {
            return browser.executeScript('localStorage.clear();');
        }).then(function () {
            return browser.get('https://' + app.fqdn + '/ghost/setup/two/');
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//input[@type="email"]')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="email"]')).sendKeys(email);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="name"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="blog-title"]')).sendKeys('Cloudron Ghost Blog');
        }).then(function () {
            return browser.findElement(by.xpath('//button[@type="submit"]')).click();
        }).then(function () {
            return browser.wait(function () {
                return browser.getCurrentUrl().then(function (currentUrl) {
                    return currentUrl === 'https://' + app.fqdn + '/ghost/setup/three/';
                });
            }, TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function login(done) {
        browser.manage().deleteAllCookies();
        browser.get('https://' + app.fqdn).then(function () {
            return browser.executeScript('localStorage.clear();');
        }).then(function () {
            return browser.get('https://' + app.fqdn + '/ghost/signin/');
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//input[@name="identification"]')), TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="identification"]')).sendKeys(email);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//button[@type="submit"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//*[text()="Welcome to Ghost"]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function checkPost(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//*[text()="Welcome to Ghost"]')));
        }).then(function () {
            done();
        });
    }

    before(function (done) {
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get user email', function (done) {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        superagent.post('https://' + inspect.apiEndpoint + '/api/v1/developer/login').send({
            username: username,
            password: password
        }).end(function (error, result) {
            if (error) return done(error);
            if (result.statusCode !== 200) return done(new Error('Login failed with status ' + result.statusCode));

            var token = result.body.token;

            superagent.get('https://' + inspect.apiEndpoint + '/api/v1/profile')
                .query({ access_token: token }).end(function (error, result) {
                if (error) return done(error);
                if (result.statusCode !== 200) return done(new Error('Get profile failed with status ' + result.statusCode));

                email = result.body.email;
                done();
            });
        });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can setup', setup);
    it('can login', login);
    it('can restart app', function (done) {
        execSync('cloudron restart --wait');
        done();
    });

    it('has existing post', checkPost);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('has existing post', function (done) {
        browser.get('https://' + app.fqdn);
        browser.wait(until.elementLocated(by.xpath('//*[text()="Welcome to Ghost"]'))).then(function () { done(); });
    });

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('has existing blog', function (done) {
        browser.wait(until.elementLocated(by.xpath('//*[text()="Welcome to Ghost"]'))).then(function () { done(); });
    });

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --new --wait --appstore-id org.ghost.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can setup', setup);
    it('can login', login);
    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('check post', checkPost);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});

