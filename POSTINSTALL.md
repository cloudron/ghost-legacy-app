Ghost is not integrated with the Cloudron user management.

After installation, you can set up an admin user and invite other users.

To do this visit the `/ghost` url.

