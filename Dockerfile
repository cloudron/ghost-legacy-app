FROM cloudron/base:0.10.0
MAINTAINER Girish Ramakrishnan <girish@cloudron.io>

USER cloudron

WORKDIR /home/cloudron
ENV VERSION 0.11.11
RUN cd /tmp && \
    wget https://github.com/TryGhost/Ghost/releases/download/${VERSION}/Ghost-${VERSION}.zip && \
    unzip Ghost-${VERSION}.zip -d /home/cloudron && \
    rm Ghost-${VERSION}.zip

ENV PATH /usr/local/node-6.9.5/bin:$PATH
RUN npm install --production

ADD start.sh /home/cloudron/start.sh

ADD ghost-config.js /home/cloudron/config.js

USER root
CMD [ "/home/cloudron/start.sh" ]
