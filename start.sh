#!/bin/bash

set -eu

if [[ ! -d "$(ls -A /app/data/content)" ]]; then
    echo "Copying files on first run (apps, data, images and themes)"
    cp -r /home/cloudron/content/. /app/data/content
fi

chown -R cloudron:cloudron /app/data

export NODE_ENV=production
exec /usr/local/bin/gosu cloudron:cloudron npm start

